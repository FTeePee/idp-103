#define led_moving 14
#define led_carrying_fuel 15
#define led_block_detected 52
#define led_block_active 51

void setup() {
  pinMode(led_moving, OUTPUT);
  pinMode(led_carrying_fuel, OUTPUT);
  pinMode(led_block_detected, OUTPUT);
  pinMode(led_block_active, OUTPUT);
}

void loop() {
  digitalWrite(led_moving, HIGH);
  delay(5000);
  digitalWrite(led_moving, LOW);
  digitalWrite(led_carrying_fuel, HIGH);
  delay(5000);
  digitalWrite(led_carrying_fuel, LOW);
  digitalWrite(led_block_detected, HIGH);
  delay(1000);
  digitalWrite(led_block_detected, LOW);
  digitalWrite(led_block_active, HIGH);
  delay(1000);
  digitalWrite(led_block_active, LOW);
}
