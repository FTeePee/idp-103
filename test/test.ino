#include "PinChangeInterrupt.h"

// https://github.com/NicoHood/PinChangeInterrupt
//    Arduino Mega:   10, 11, 12, 13, 50, 51, 52, 53, A8 (62), A9 (63), A10 (64),
// (software ints.)   A11 (65), A12 (66), A13 (67), A14 (68), A15 (69)

bool state = LOW;
unsigned long start = 0;

void setup() {
  Serial.begin(9600);
  pinMode(3, INPUT);
  pinMode(4, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(3), ultrasonic, CHANGE);
}

void ultrasonic(void) {
  //Serial.println("Something received");
  if(state) {
    Serial.println((micros() - start));
  } else {
    start = micros();
  }
  state = !state;
}

void ultrasonic_start(void) {
  Serial.println("Pulse sent");
  digitalWrite(4, HIGH);
  delayMicroseconds(12);
  digitalWrite(4, LOW);
}

void loop() {
  ultrasonic_start();
  delay(250);
}

