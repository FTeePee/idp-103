#define hallEffect 1
#define lineSensor 3

void setup() {
  pinMode(hallEffect, INPUT);
  pinMode(lineSensor, INPUT);
}

void loop() {
  Serial.print("Hall effect: ");
  Serial.println(digitalRead(hallEffect));
  Serial.print("Line sensor: ");
  Serial.println(digitalRead(lineSensor));
}
