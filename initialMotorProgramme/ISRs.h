/*
 * Our Pin ISRs (Interupt Service Routines) 
 * These incrememt counters or set flags which are used in our code
 */
void rightEncoderISR() {
  if(millis() - encoderRightLastCall > 20) {
    rightEncoder += 1;
  }
  encoderRightLastCall = millis();
}

void leftEncoderISR() {
  if(millis() - encoderLeftLastCall > 20) {
    leftEncoder += 1;
  }
  encoderLeftLastCall = millis();
}

void lightGateISR() {
  if(millis() - lightGateLastCall > 1000) {
    lightGateBlocked = 1;
    lightGateLastCall = millis();
  }
}

void frontRightBumperISR() {
   frontRightBumper = !frontRightBumper;
}

void frontLeftBumperISR() {
  frontLeftBumper = !frontLeftBumper;
}

void rearRightBumperISR() {
  rearRightBumper = !rearRightBumper;
}

void rearLeftBumperISR() {
  rearLeftBumper = !rearLeftBumper;
}
