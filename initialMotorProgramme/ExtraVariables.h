// ISR Variables
volatile int rightEncoder = 0;
volatile int leftEncoder = 0;
volatile bool lineSensor = LOW;
volatile bool lightGateBlocked = 0;
volatile bool blockOnBoard = LOW; 
volatile bool dangerDetected = LOW;
volatile bool frontRightBumper = LOW;
volatile bool frontLeftBumper = LOW;
volatile bool rearRightBumper = LOW;
volatile bool rearLeftBumper = LOW;
volatile long encoderLeftLastCall = 0;
volatile long encoderRightLastCall = 0;
volatile long lightGateLastCall = -10000;
volatile bool insideLightFunction = 0;
bool sideGateOpen = 0;
bool rearGateOpen = 1;

const float pi = 3.141592;

// Dimensions
const int encoderSteps = 36;//I've doubled this
const int wheelDiameter = 100;
const int wheelCircumference = pi * wheelDiameter;
const int wheelBase = 266+1;
const int robotWidth = 300;
const int robotLength = 361;
const int fullRotation = encoderSteps * wheelBase * pi * 2 / wheelCircumference;
const int reverseDistance = robotLength * 0.7;

// Code Variables
bool encoderWorking = LOW;
int state = 2; // 0: no state, 1: driving towards blocks (can collect), 2: space filling curve, 3: return blocks
int sidePos = 0;
int rearPos = 0;
int spaceFillingCount = 0;
int standardSpeed = 127;
int speedChanged = LOW;
int diff = 0;
