#include <Wire.h>
#include <Adafruit_MotorShield.h>
#include <PinChangeInterrupt.h>
#include <Servo.h>
#include "Pins.h"
#include "ExtraVariables.h"
#include "ISRs.h"

// Create the motor shield object with the default I2C address & initalise motors and servos
Adafruit_MotorShield AFMS = Adafruit_MotorShield();
Adafruit_DCMotor *leftMotor = AFMS.getMotor(3);
Adafruit_DCMotor *rightMotor = AFMS.getMotor(1);
Servo sideServo;
Servo rearServo;

void setup() {
  Serial.begin(9600);         
  Serial.println("--- Begin setup ---");

  //Start the Motor shield, attach the Servos and test them. 
  AFMS.begin();

  sideServo.attach(10);
  rearServo.attach(9);

  sideServo.write(0);
  rearServo.write(0);

  // Set all the pins
  pinMode(frontRightBumperPin, INPUT);
  pinMode(frontLeftBumperPin, INPUT);
  pinMode(rearRightBumperPin, INPUT);
  pinMode(rearLeftBumperPin, INPUT);

  pinMode(blockDetectedPin, OUTPUT);
  pinMode(blockDangerPin, OUTPUT);
  pinMode(robotMovingPin, OUTPUT);
  pinMode(carryingFuelPin, OUTPUT);

  // Attach all the interupt pins to their ISRs (Interupt Service Routines)
  attachPCINT(digitalPinToPCINT(frontRightBumperPin), frontRightBumperISR, CHANGE);
  attachPCINT(digitalPinToPCINT(frontLeftBumperPin), frontLeftBumperISR, CHANGE);
  attachPCINT(digitalPinToPCINT(rearRightBumperPin), rearRightBumperISR, CHANGE);
  attachPCINT(digitalPinToPCINT(rearLeftBumperPin), rearLeftBumperISR, CHANGE);

  attachPCINT(digitalPinToPCINT(leftEncoderPin), leftEncoderISR, CHANGE);
  attachPCINT(digitalPinToPCINT(rightEncoderPin), rightEncoderISR, CHANGE);
  attachPCINT(digitalPinToPCINT(lightGatePin), lightGateISR, RISING);

  // make sure the gates are set in the right place
  closeSideGate();
  closeRearGate();

  Serial.println("--- End setup ---");
}

void loop() {
  // hit the rear bumper to get going
  if(rearLeftBumper) {
    testEncoders();
    mainLoop();
  } 
}

void mainLoop() { // the heart of the routing Algorithm
  //---- Initial Orientation -----
  turnLockedWheel("clockwise", 90);
  goUntilBump("forward");
  go("reverse", reverseDistance);
  turnLockedWheel("anticlockwise", 90);
  goUntilBump("reverse");
  //---- Drive to the top right ----
  goUntilBump("forward");
  go("reverse", reverseDistance * 0.9);
  turnLockedWheel("anticlockwise", 90);
  goUntilBump("reverse");
  
  // space filling curve loop
  for(int i = 0; i < 3; i++) {
    goUntilBump("forward");
    go("reverse", reverseDistance);
    if(i % 2 == 0) {
      turnLockedWheel("anticlockwise",180);
    } else {
      turnLockedWheel("clockwise",180);
    }
    goUntilBump("reverse");
  }
  // return the blocks to the safe zone
  goUntilBump("forward");
  go("reverse", reverseDistance*1.5);
  turnLockedWheel("clockwise", 90);
  goUntilBump("forward");
  go("reverse", reverseDistance*0.9);
  turnLockedWheel("clockwise", 90);
  goUntilBump("reverse");
  go("forward", 1000);
  openRearGate();
  digitalWrite(carryingFuelPin, LOW);
  goUntilBump("forward");
  // Get into box
  closeRearGate();
  go("reverse", reverseDistance / 2);
  turnLockedWheel("clockwise", -90);
  digitalWrite("robotMovingPin", LOW);
}

void openSideGate() {
  // opens the side gate to let blocks in
  sideServo.attach(sideDoorPin);
  for(sidePos = 120; sidePos>=0; sidePos -= 1) {
    sideServo.write(sidePos);
    delay(2);
  }
  delay(100);
  sideServo.detach();
  digitalWrite(sideDoorPin, LOW);
}

void closeSideGate() {
  // closes the side gate to reject blocks
  sideServo.attach(sideDoorPin);
  for(sidePos = 0; sidePos<=120; sidePos += 1) {
    sideServo.write(sidePos);  
    // Serial.println(sidePos);
    delay(2);
  }
  delay(100);
  sideServo.detach();
  digitalWrite(sideDoorPin, LOW);
}

void closeSideGateNoLoop() {
  // resets the side gate to reject blocks just incase it had been caught
  sideServo.attach(sideDoorPin);
  for(sidePos = 70; sidePos<=120; sidePos += 1) {
    sideServo.write(sidePos);  
    // Serial.println(sidePos);
    delay(1);
  }
  delay(100);
  sideServo.detach();
  digitalWrite(sideDoorPin, LOW);
}

void openRearGate() {
  // opens the rear gate to let the blocks out
  rearServo.attach(rearDoorPin);
  for(rearPos = 0; rearPos <= 90; rearPos += 1) {
    rearServo.write(rearPos);  
    // Serial.println(rearPos);
    delay(5);
  }
  rearServo.detach();
  digitalWrite(rearDoorPin, LOW);
}

void closeRearGate() {
 // closes the rear gate - preventing blocks from leaving
  rearServo.attach(rearDoorPin);
  for(rearPos = 120; rearPos>=0; rearPos -= 1) {
    rearServo.write(rearPos);  
    // Serial.println(rearPos);
    delay(5);
  }
  rearServo.detach();
  digitalWrite(rearDoorPin, LOW);
}

void turnLockedWheel(String direction, long degrees) {
  /* 
   *  turns the robot with one wheel locked 
   *  args: 
   *    direction: string - anticlockwise or clockwise 
   *    degrees: int - number of degrees in turn eg 90 = quater turn
  */
  digitalWrite(robotMovingPin, HIGH);
  if(!encoderWorking) {
    long timeToTurn = (2 * 9000 * degrees) / 360;
    if(direction == "clockwise") {
      if(degrees > 0) {
        rightMotor->run(BACKWARD);
        leftMotor->run(FORWARD);
      } else {
        rightMotor->run(FORWARD);
        leftMotor->run(BACKWARD);
        degrees = -degrees;
      }
      rightMotor->setSpeed(0);
      leftMotor->setSpeed(standardSpeed);
      delay(timeToTurn * 127 / standardSpeed);
      leftMotor->setSpeed(0);    
    } else {
      if(degrees > 0) {
        rightMotor->run(FORWARD);
        leftMotor->run(BACKWARD);
      } else {
        rightMotor->run(BACKWARD);
        leftMotor->run(FORWARD);
        degrees = -degrees;
      }
      leftMotor->setSpeed(0);
      rightMotor->setSpeed(standardSpeed-2);
      delay(timeToTurn * 127 / standardSpeed);
      rightMotor->setSpeed(0);  
    }
  } else {
    leftEncoder = 0;
    rightEncoder = 0;
    if(direction == "clockwise") {
      rightMotor->run(BACKWARD);
      leftMotor->run(FORWARD);
      rightMotor->setSpeed(0);
      leftMotor->setSpeed(standardSpeed);
      while(leftEncoder < fullRotation * degrees / 360) {
        delay(10);
      }
      leftMotor->setSpeed(0);    
    } else {
      leftMotor->run(BACKWARD);
      rightMotor->run(FORWARD);
      leftMotor->setSpeed(0);
      rightMotor->setSpeed(standardSpeed-2);
      while(rightEncoder < fullRotation * degrees / 360) {
        delay(10);
      }
      rightMotor->setSpeed(0);  
    }
  }
  digitalWrite(robotMovingPin, LOW);
}

void turnInPlace(String direction, long degrees) {
  /* 
   *  turns the robot with one wheel going forwards and the other going backwards 
   * args:
   *  direction: string - "anticlockwise" or "clockwise"
   *  degrees: int - number of degrees in turn eg 90 = quater turn
   */
  digitalWrite(robotMovingPin, HIGH);
  if(!encoderWorking) {
    long timeToTurn = (2 * 8140 * degrees) / 360;
    if(direction == "clockwise") {
      rightMotor->run(BACKWARD);
      leftMotor->run(FORWARD);
      rightMotor->setSpeed(standardSpeed);
      leftMotor->setSpeed(standardSpeed);
      delay(timeToTurn * 127 / (2 * standardSpeed));  
    } else {
      leftMotor->run(BACKWARD);
      rightMotor->run(FORWARD);
      leftMotor->setSpeed(standardSpeed);
      rightMotor->setSpeed(standardSpeed);
      delay(timeToTurn * 127 / (2 * standardSpeed));
    }
    rightMotor->setSpeed(0);
    leftMotor->setSpeed(0);
  } else {
    leftEncoder = 0;
    rightEncoder = 0;
    if(direction == "clockwise") {
      rightMotor->run(BACKWARD);
      leftMotor->run(FORWARD);
      rightMotor->setSpeed(0);
      leftMotor->setSpeed(standardSpeed);
      while(leftEncoder < fullRotation * degrees / 360) {
        delay(10);
      }
      leftMotor->setSpeed(0);    
    } else {
      leftMotor->run(BACKWARD);
      rightMotor->run(FORWARD);
      leftMotor->setSpeed(0);
      rightMotor->setSpeed(standardSpeed);
      while(rightEncoder < fullRotation * degrees / 360) {
        delay(10);
      }
      rightMotor->setSpeed(0);  
    }
  }
  digitalWrite(robotMovingPin, LOW);
}

void goUntilBump(String direction) {
  /* 
   *  drives the robot until both touch sensors are pressed
   * args: 
   *   direction: string - "forward" or "backward"
   */
  leftEncoder = 0;
  rightEncoder = 0;
  digitalWrite(robotMovingPin, HIGH);
  if(direction == "forward") {
    leftMotor->run(FORWARD);
    rightMotor->run(FORWARD);
    rightMotor->setSpeed(standardSpeed-2);
    leftMotor->setSpeed(standardSpeed);\
    
    // while loop that runs while robot is moving forward
    while(!frontRightBumper or !frontLeftBumper) {

      lightGateCheck();

      //cut the motor speed if one side hits to aid lining up 
      if(frontRightBumper) {
        rightMotor->setSpeed(standardSpeed/4);
      } else if(frontLeftBumper) {
        leftMotor->setSpeed(standardSpeed/4);
      } else if(encoderWorking) {
        
        // System using encoders to keep straight 
        diff = abs(rightEncoder-leftEncoder);
        if(diff > 0) {
          if(rightEncoder>leftEncoder) {
            rightMotor->setSpeed(standardSpeed-2-diff*2);
          } else {
            leftMotor->setSpeed(standardSpeed-diff*2);
          }
          speedChanged = HIGH;
        } else if(speedChanged) {
          rightMotor->setSpeed(standardSpeed-2);
          leftMotor->setSpeed(standardSpeed);
          speedChanged = LOW;
        }
      }
      delay(10);
    }
    delay(500);
  } else {
    leftMotor->run(BACKWARD);
    rightMotor->run(BACKWARD);
    rightMotor->setSpeed(standardSpeed-2);
    leftMotor->setSpeed(standardSpeed);
    while(!rearRightBumper or !rearLeftBumper) {
      if(rearRightBumper) {
        rightMotor->run(FORWARD);
        rightMotor->setSpeed(standardSpeed/8);
      } else if(rearLeftBumper) {
        leftMotor->setSpeed(standardSpeed/16);
      }
      delay(10);
    }
    delay(100);
  }
  digitalWrite(robotMovingPin, LOW);
  rightMotor->setSpeed(0);
  leftMotor->setSpeed(0);
}

void go(String direction, long distance) {
  /*
   * drives the robot a certain distance
   * args: 
   *    distance: int - distance to move the robot in mm
   */
  digitalWrite(robotMovingPin, HIGH);
  if(!encoderWorking) {
    if(direction == "forward") {
      leftMotor->run(FORWARD);
      rightMotor->run(FORWARD);
      rightMotor->setSpeed(standardSpeed-2);
      leftMotor->setSpeed(standardSpeed);
      delay(distance * 10.6);
    } else {
      leftMotor->run(BACKWARD);
      rightMotor->run(BACKWARD);
      rightMotor->setSpeed(standardSpeed-2);
      leftMotor->setSpeed(standardSpeed);
      delay(distance * 10.6);
    }
  } else {
    distance = encoderSteps * distance / wheelCircumference;
    leftEncoder = 0;
    rightEncoder = 0;
    if(direction == "forward") {
      leftMotor->run(FORWARD);
      rightMotor->run(FORWARD);
      rightMotor->setSpeed(standardSpeed-2);
      leftMotor->setSpeed(standardSpeed);
      while(rightEncoder <= distance) {
        delay(1);
      }
    } else {
      leftMotor->run(BACKWARD);
      rightMotor->run(BACKWARD);
      rightMotor->setSpeed(standardSpeed-2);
      leftMotor->setSpeed(standardSpeed);
      while(rightEncoder <= distance) {
        delay(1);
      }    
    }
  }
  rightMotor->setSpeed(0);
  leftMotor->setSpeed(0);
}

void lightGateCheck() {
  /*
   * prcesses a block in the funnel
   */
   // checks if there is a block to process
    if(lightGateBlocked && !insideLightFunction) { 
      Serial.println("Found a block!");
      insideLightFunction = 1;
      digitalWrite(blockDetectedPin,HIGH);
      rightMotor->setSpeed(standardSpeed / 5 - 1);
      leftMotor->setSpeed(standardSpeed / 5);
      // polls the hall effect sensor
      int hallEffectCount = 0;
      for(int i = 0; i < 1800; i++) {
        hallEffectCount += digitalRead(hallEffectPin);
        delay(1);
      }
      Serial.println(hallEffectCount);
      if(hallEffectCount > 30) {
        digitalWrite(blockDangerPin, HIGH);
        Serial.println("Was magnetic");
        closeSideGate();
        delay(1000);
      } else {
        Serial.println("Wasn't magnetic");
        openSideGate();
        long startDelay = millis() + 3000;
        while(millis() < startDelay) {
          delay(1);
        }
        closeSideGate();  
        digitalWrite(carryingFuelPin, HIGH);
      }
      digitalWrite(blockDetectedPin,LOW);
      digitalWrite(blockDangerPin, LOW);
      rightMotor->setSpeed(standardSpeed-2);
      leftMotor->setSpeed(standardSpeed);
      lightGateBlocked = 0;
      insideLightFunction = 0;
    }
}

void testEncoders() {
  // Tests if the encoders are working and sets a global varialbe  
  go("forward", reverseDistance);
  go("backward", reverseDistance);
  delay(100);

  if(rightEncoder == 0 or leftEncoder == 0) {
    Serial.println("!!! ENCODERS NOT FUNCTIONING !!!");
  } else {
    Serial.println("--- ENCODERS ARE WORKING ---");
    encoderWorking = HIGH;
  }
}
