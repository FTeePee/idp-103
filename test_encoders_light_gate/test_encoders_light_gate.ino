#include <PinChangeInterrupt.h>

#define encoderLeft A10
#define encoderRight A9
#define lightGate A8

volatile long encoderLeftDistance = 0;
volatile long encoderRightDistance = 0;
volatile bool lightGateBlocked;
volatile long encoderLeftLastCall = 0;
volatile long encoderRightLastCall = 0;
volatile long lightGateLastCall = 0;

void lightGateStatus(void) {
  lightGateBlocked = !lightGateBlocked;
  Serial.print("Light gate status: ");
  Serial.println(lightGateBlocked);
}

void encoderLeftInterrupt(void) {
  if(millis() - encoderLeftLastCall > 20) {
    encoderLeftDistance += 1;
    Serial.print("Encoder left: ");
    Serial.println(encoderLeftDistance);
  }
  encoderLeftLastCall = millis();
}

void encoderRightInterrupt(void) {
  if(millis() - encoderRightLastCall > 20) {
    encoderRightDistance += 1;
    Serial.print("Encoder right: ");
    Serial.println(encoderRightDistance);
  }
  encoderRightLastCall = millis();
}

void lightGateInterrupt(void) {
  if(millis() - lightGateLastCall > 2000) {
    Serial.println("Light gate interrupted");
  }
  lightGateLastCall = millis();
}

void setup() {
  Serial.begin(9600);
  pinMode(encoderLeft, INPUT);
  pinMode(encoderRight, INPUT);
  pinMode(lightGate, INPUT);
  pinMode(36, INPUT);
  attachPCINT(digitalPinToPCINT(encoderLeft), encoderLeftInterrupt, CHANGE);
  attachPCINT(digitalPinToPCINT(encoderRight), encoderRightInterrupt, CHANGE);
  attachPCINT(digitalPinToPCINT(lightGate), lightGateInterrupt, RISING);
}

void loop() {
  // Serial.println(digitalRead(36));
  // delay(50);
}
