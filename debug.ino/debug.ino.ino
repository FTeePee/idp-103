// Install Pin change interrupt for a pin, can be called multiple times

int count = 0;
 
void pciSetup(byte pin)
{
    *digitalPinToPCMSK(pin) |= bit (digitalPinToPCMSKbit(pin));  // enable pin
    PCIFR  |= bit (digitalPinToPCICRbit(pin)); // clear any outstanding interrupt
    PCICR  |= bit (digitalPinToPCICRbit(pin)); // enable interrupt for the group
}
 
// Use one Routine to handle each group
 
ISR (PCINT0_vect) // handle pin change interrupt for D8 to D13 here
 {    
     Serial.println(count);
     count += 1;
 }
 
ISR (PCINT2_vect) // handle pin change interrupt for D0 to D7 here
 {
     Serial.println(count);
     count += 1;
 }  

void setup() {
  Serial.begin(9600);
  // enable interrupt for pin...
  pciSetup(13);
  pciSetup(7);
  pciSetup(6);
  pciSetup(5);
}
 
 
void loop() {
  // Nothing needed
  Serial.println("Keep alive");
  Serial.println(digitalRead(8));
  delay(1000);
}
