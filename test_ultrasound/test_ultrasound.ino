#include "PinChangeInterrupt.h"

// https://github.com/NicoHood/PinChangeInterrupt
//    Arduino Mega:   10, 11, 12, 13, 50, 51, 52, 53, A8 (62), A9 (63), A10 (64),
// (software ints.)   A11 (65), A12 (66), A13 (67), A14 (68), A15 (69)

#define ultrasound1_trig 4
#define ultrasound1_echo 3
#define ultrasound2_trig 17
#define ultrasound2_echo 18

volatile bool state1 = LOW;
volatile unsigned long start1 = 0;
volatile bool state2 = LOW;
volatile unsigned long start2 = 0;
const int ultrasonic_interval = 100;
volatile int ultrasonic_timer = 0;
volatile unsigned long dist1 = 0;
volatile unsigned long dist2 = 0;

void setup() {
  Serial.begin(9600);
  pinMode(ultrasound1_echo, INPUT);
  pinMode(ultrasound1_trig, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(ultrasound1_echo), ultrasonic1, CHANGE);

  pinMode(ultrasound2_echo, INPUT);
  pinMode(ultrasound2_trig, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(ultrasound2_echo), ultrasonic2, CHANGE);

  // Timer0 is already used for millis() - we'll just interrupt somewhere
  // in the middle and call the "Compare A" function below
  OCR0A = 0xAF;
  TIMSK0 |= _BV(OCIE0A);
}

SIGNAL(TIMER0_COMPA_vect) 
{
  ultrasonic_timer = (ultrasonic_timer + 1) % ultrasonic_interval;
  if(ultrasonic_timer == 0) {
    ultrasonic_start();
  }
}

void ultrasonic1(void) {
  Serial.println("Something received");
  state1 = !state1;
  if(!state1) {
    double dist1tmp = micros() - start1;
    if(dist1tmp < 12000 && dist1tmp > 0) {
      dist1 = (dist1 + dist1tmp/5.8) / 2;
    }
    state1 = digitalRead(ultrasound1_echo);
  } else {
    start1 = micros();
  }
}

void ultrasonic2(void) {
  Serial.println("Something received");
  state2 = !state2;
  if(!state2) {
    double dist2tmp = micros() - start2;
    if(dist2tmp < 12000 && dist2tmp > 0) {
      dist2 = (dist2 + dist2tmp/5.8) / 2;
    }
    state2 = digitalRead(ultrasound2_echo);
  } else {
    start2 = micros();
  }
}

void ultrasonic_start(void) {
  // Serial.println("Pulse sent");
  digitalWrite(ultrasound1_trig, HIGH);
  digitalWrite(ultrasound2_trig, HIGH);
  delayMicroseconds(12);
  digitalWrite(ultrasound1_trig, LOW);
  digitalWrite(ultrasound2_trig, LOW);
}

void loop() {
  Serial.println(dist2);
  delay(ultrasonic_interval);
}

