/*
* Ultrasonic Sensor HC-SR04 and Arduino Tutorial
*
* by Dejan Nedelkovski,
* www.HowToMechatronics.com
*
*/

// defines pins numbers
const int trigPin = 4;
const int echoPin = 3;

// defines variables
volatile int distance;
volatile unsigned long tim;

void ultraSonicISR() {
  distance= (tim- micros())*0.034/2000;
}

void setup() {
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  attachInterrupt(digitalPinToInterrupt(echoPin), ultraSonicISR, CHANGE); 
  Serial.begin(9600); // Starts the serial communication
}

void loop() {
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  tim = micros();
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  
  // Reads the echoPin, returns the sound wave travel time in microseconds --- uh yeah hoping this works
  // Calculating the distance
  delay(1000);
  // Prints the distance on the Serial Monitor
  // Serial.print("Distance: ");
  Serial.println(distance);
}
