#include <PinChangeInterrupt.h>

#define bumper_front_left 53
#define bumper_front_right 11
#define bumper_back_left 13
#define bumper_back_right 12

int count = 0;

void setup() {
  Serial.begin(9600);
  
  pinMode(bumper_front_left, INPUT);
  pinMode(bumper_front_right, INPUT);
  pinMode(bumper_back_left, INPUT);
  pinMode(bumper_back_right, INPUT);

  attachPCINT(digitalPinToPCINT(bumper_front_left), intISR, RISING);
  attachPCINT(digitalPinToPCINT(bumper_front_right), intISR, RISING);
  attachPCINT(digitalPinToPCINT(bumper_back_left), intISR, RISING);
  attachPCINT(digitalPinToPCINT(bumper_back_right), intISR, RISING);
}

void intISR(void) {
  Serial.println(count);
  count += 1;
}

void loop() {

}
