#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif

#if defined(__AVR_ATmega168__) || defined(__AVR_ATmega8__) || defined(__AVR_ATmega328P__)
  // deactivate internal pull-ups for twi
  // as per note from atmega8 manual pg167
  cbi(PORTC, 4);
  cbi(PORTC, 5);
#else
  // deactivate internal pull-ups for twi
  // as per note from atmega128 manual pg204
  cbi(PORTD, 0);
  cbi(PORTD, 1);
#endif

#define encoderLeft 19
#define encoderRight 20
#define lightGate 21

volatile long encoderLeftDistance;
volatile long encoderRightDistance;
volatile bool lightGateBlocked;

void incrementLeftEncoder(void) {
  encoderLeftDistance += 1;
  Serial.print("Encoder left: ");
  Serial.println(encoderLeftDistance);
}

void incrementRightEncoder(void) {
  encoderRightDistance += 1;
  Serial.print("Encoder right: ");
  Serial.println(encoderRightDistance);
}

void lightGateStatus(void) {
  lightGateBlocked = !lightGateBlocked;
  Serial.print("Light gate status: ");
  Serial.println(lightGateBlocked);
}


void setup() {
  Serial.begin(9600);
  pinMode(encoderLeft, INPUT);
  pinMode(encoderRight, INPUT);
  pinMode(lightGate, INPUT);
  attachInterrupt(digitalPinToInterrupt(encoderLeft), incrementLeftEncoder, CHANGE);
  attachInterrupt(digitalPinToInterrupt(encoderRight), incrementRightEncoder, CHANGE);
  attachInterrupt(digitalPinToInterrupt(lightGate), lightGateStatus, CHANGE);
  // digitalWrite(SDA, 0);
  // digitalWrite(SCL, 0);
}

void loop() {
  Serial.println(digitalRead(encoderLeft));
  delay(50);
}
